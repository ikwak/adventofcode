#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <sstream>
#include <iterator>
#include <set>

void part1() {
    std::string fname = "data/day1.txt";
    std::ifstream input_stream(fname);
    if(!input_stream) {
        std::cout << "error" << std::endl;
        return;
    }

    std::string line;
    int sum = 0;
    while(std::getline(input_stream, line)) {
        // int value = 0;
        // sscanf(line.c_str(), "%d", &value);
        int value = std::stoi(line);
        sum += value;
    }
    std::cout << sum << std::endl;
}

void part2() {
    std::string fname = "data/day1.txt";
    std::ifstream input_stream(fname);
    if(!input_stream) {
        std::cout << "error" << std::endl;
        return;
    }
    // std::stringstream string_buffer(
    //     "-6\n+3\n+8\n+5\n-6"
    // );

    std::string line;
    std::vector<int> frequencies;
    while(std::getline(input_stream, line)) {
        frequencies.push_back(std::stoi(line));
    }

    std::set<int> seen_freqs;
    int sum = 0;
    while(true) {
        // loop over frequencies to sum things up.
        for(auto const& it: frequencies) {
            sum += it;
            if(seen_freqs.count(sum) > 0) {
                std::cout << sum << std::endl;
                return;
            }
            else {
                seen_freqs.insert(sum);
            }
        }
    }

    return;
}

void main() {
    part1();
    part2();
}