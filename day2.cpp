#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <sstream>
#include <iterator>


unsigned int hamming_dist(const std::string str1, const std::string str2, std::string& result) {
    // for(auto it1: str1, auto it2: str2) {
    unsigned int dist = 0;
    result = "";
    for(int i = 0; i < str1.length(); ++i) {
        //cout << it1 << " " << it2 << std::endl;
        // std::cout << str1[i] << " " << str2[i] << std::endl;
        if(str1[i] != str2[i]) {
            ++dist;
        } else {
            result += str1[i];
        }
    }
    // std::cout << std::endl << dist << std::endl;

    if(dist == 1) {
        std::cout << str1 << std::endl << str2 << std::endl;
    }

    return dist;
}


void part2() {
    std::string fname = "data/day2.txt";
    std::ifstream input_stream(fname);
    if(!input_stream) {
        std::cout << "error" << std::endl;
        return;
    }

    std::string line;
    std::vector<std::string> all_lines;
    while(std::getline(input_stream, line)) {
        all_lines.push_back(line);
    }

    std::string result = "";
    for(int i = 0; i < all_lines.size(); ++i) {
        for(int j = i + 1; j < all_lines.size(); ++j)
        {
            if(hamming_dist(all_lines[i], all_lines[j], result) == 1) {
                std::cout << result << std::endl;
                break;
            }
        }
    }

    return;
}


void part1() {
    std::string fname = "data/day2.txt";
    std::ifstream input_stream(fname);
    if(!input_stream) {
        std::cout << "error" << std::endl;
        return;
    }

    unsigned int doubles = 0;
    unsigned int triples = 0;
    std::string line;
    while(std::getline(input_stream, line)) {
        //std::cout << line << std::endl;
        // loop over the characters
        std::vector<unsigned int> char_count(26, 0);
        for(auto const& it: line) {
            int ascii_val = int(it) - int('a');
            ++char_count[ascii_val];
        }

        // loop over alphabet to check for doubles or triples;
        bool double_found = false;
        bool triple_found = false;
        char curr = 'a';
        for(auto const& it: char_count) {
            if(double_found == false && it == 2) {
                //std::cout << curr << " double" << std::endl;
                ++doubles;
                double_found = true;
            }
            if(triple_found == false && it == 3) {
                //std::cout << curr << " triple" << std::endl;
                ++triples;
                triple_found = true;
            }
            ++curr;
        }

        // curr = 'a';
        // for(auto const& it: char_count) {
        //     std::cout << curr << " ";
        //     std::cout << it << std::endl;
        //     ++curr;
        // }
        //return;
    }

    std::cout << doubles * triples << std::endl;
    return;
}


void main() {
    part1();
    part2();

    return;
}
