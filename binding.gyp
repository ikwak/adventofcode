{
  "targets": [
    {
      "target_name": "day1",
      "type": "executable",
      "sources": [
        "day1.cpp"
      ],
    },
    {
      "target_name": "day2",
      "type": "executable",
      "sources": [
        "day2.cpp"
      ],
    },
  ]
}